FROM azadehafzarhub/gitlab-ci-ruby-build:latest

LABEL maintainer="Mohammad Mahdi Baghbani Pourvahid <MahdiBaghbani@protonmail.com>"

# get latest version tag and install nvm.
RUN LATEST_VERSION=$(curl -s https://api.github.com/repos/nvm-sh/nvm/releases/latest \
                | grep "tag_name" \
                | awk '{print substr($2, 2, length($2)-3)}') \
    && curl -o- "https://raw.githubusercontent.com/nvm-sh/nvm/$LATEST_VERSION/install.sh" | bash

# export paths to bash_profile.
RUN echo "export NVM_DIR='$HOME/.nvm'" >> ~/.bash_profile
RUN echo "[ -s '$NVM_DIR/nvm.sh' ] && \. '$NVM_DIR/nvm.sh'" >> ~/.bash_profile
RUN echo "[ -s '$NVM_DIR/bash_completion' ] && \. '$NVM_DIR/bash_completion'" >> ~/.bash_profile
RUN source ~/.bash_profile

# install nodejs.
RUN nvm install node

# install surge.
RUN npm install --global surge
